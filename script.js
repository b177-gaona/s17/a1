/*
1. In the S17 folder, create an a1 folder and an index.html and script.js file inside of it.
2. Link the script.js file to the index.html file.
3. Create an addStudent() function that will accept a name of the student and add it to the student array.
4. Create a countStudents() function that will print the total number of students in the array.
5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.
6. Create a findStudent() function that will do the following:
Search for a student name when a keyword is given (filter method).
- If one match is found print the message studentName is an enrollee.
- If multiple matches are found print the message studentNames are enrollees.
- If no match is found print the message studentName is not an enrollee.
- The keyword given should not be case sensitive.
7. Create a gitlab project repository named a1 in S17.
8. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
9. Add the link in Boodle.
*/


let studentArray = [];

//3. Create an addStudent() function that will accept a name of the student and add it to the student array.
function addStudent (newElement) {
	studentArray.push(newElement);
	console.log(newElement + " was added to the student's list.");
//	console.log(studentArray);
}

//4. Create a countStudents() function that will print the total number of students in the array.
function countStudents(){
	console.log("There are a total of " + studentArray.length + " students enrolled.")
}

//5. Create a printStudents() function that will sort and individually print the students (sort and forEach methods) in the array.
function printStudents(){
	studentArray.sort();
	studentArray.forEach(student => console.log(student));
}

/*
6. Create a findStudent() function that will do the following:
Search for a student name when a keyword is given (filter method).
- If one match is found print the message studentName is an enrollee.
- If multiple matches are found print the message studentNames are enrollees.
- If no match is found print the message studentName is not an enrollee.
- The keyword given should not be case sensitive.
*/
function findStudent(someString){
	let filteredStudents = studentArray.filter(function(studentArray){
		return studentArray.toLowerCase().includes(someString.toLowerCase());
	})
	if (filteredStudents.length === 0){
		console.log(someString + " is not an enrollee");
	}
	else if (filteredStudents.length === 1){
		console.log(someString + " is an enrollee");
	}
	else 
	console.log(filteredStudents + " are enrollees");
}
